"use strict";

// トップに戻るボタン
$('.to-top').click(function () {
  $('body,html').animate({
    scrollTop: 0
  }, 800);
  return false;
}); // ハンバーガーメニューの開閉

navFunc = function navFunc() {
  document.querySelector('header').classList.toggle('open');
};