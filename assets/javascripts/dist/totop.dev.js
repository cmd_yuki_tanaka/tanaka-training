"use strict";

// トップに戻るボタン
$('.to-top').click(function () {
  $('body,html').animate({
    scrollTop: 0
  }, 800);
  return false;
});

function navFunc() {
  document.querySelector('html').classList.toggle('open');
}