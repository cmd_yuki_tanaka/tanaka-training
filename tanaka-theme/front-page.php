    <?php get_header(); ?>
      <div class="keyvisual">
        <div class="swiper-container">
          <div class="swiper-wrapper">
            <?php
              $args = [
                'post_type' => 'slider', // カスタム投稿名
                'posts_per_page' => 10, // 表示する数
              ];
              $my_query = new WP_Query($args);
            ?>
            <?php if ( $my_query->have_posts() ) : ?>
              <?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
                <?php
                  $pic = get_field('pic');
                  $pic_url = $pic['sizes']['large'];
                ?>
                <div class="swiper-slide">
                  <img src="<?php echo $pic_url ?>" alt="" class="swiper-image">
                </div>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="services">
        <div class="services-wrapper"><a class="service navy" href="#">
            <div class="service__title">
              <p>相続税のお悩み</p>
            </div>
            <div class="service__icon">
              <figure><img src="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/images/icon1.png" alt="アイコン（相続税のお悩み）"></figure>
            </div>
            <div class="service__explanation"> 
              <ul>
                <li>・どのくらいの税額？</li>
                <li>・売却したい</li>
                <li>・料金について</li>
              </ul>
            </div></a><a class="service blue" href="#">
            <div class="service__title">
              <p>顧問契約</p>
            </div>
            <div class="service__icon">
              <figure><img src="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/images/icon2.png" alt="アイコン（顧問契約）"></figure>
            </div>
            <div class="service__explanation"> 
              <ul>
                <li>・個人→法人化する</li>
                <li>・経営に関する税務をサポート</li>
                <li>　</li>
              </ul>
            </div></a><a class="service lightblue" href="./question.html">
            <div class="service__title">
              <p>よくあるご質問</p>
            </div>
            <div class="service__icon">
              <figure><img src="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/images/icon3.png" alt="アイコン（よくあるご質問）"></figure>
            </div>
            <div class="service__explanation"> 
              <ul>
                <li>・原稿はダミーです。</li>
                <li>・ダミーですダミーです。</li>
                <li>・ダミーです。</li>
              </ul>
            </div></a></div>
      </div>
      <div class="message">
        <div class="container">
          <div class="title"> 
            <p>豊富な法律知識と実績から</p>
            <p>最善の解決方法を</p>
            <p>ご提案します</p>
          </div>
          <div class="detail">
            <p>税理士は各種税金・税務申告に関する問題をはじめ、</p>
            <p>会社の経理や経営相談、相続に代表されるご家族の悩みなどを相談できる専門家です。</p>
            <p>相続問題、独立・企業をお考えの方へのサポート、法人・個人事業主の税務申告・</p>
            <p>会計業務と経営支援を業務の中心として取り組んでおります。</p>
            <p>フットワーク軽く、一緒に考え、一緒に悩み、皆様の不安を解決して参ります。</p>
            <p>一人で悩まず、まずはご相談下さい。</p>
          </div>
        </div>
      </div>
      <div class="heroimage"><img src="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/images/hero.jpg" alt=""></div>
      <div class="news">
        <div class="container">
          <div class="title">NEWS</div>
          <div class="details">
            <?php
              $args = [
                'post_type' => 'news', // カスタム投稿名
                'posts_per_page' => 3, // 表示する数
              ];
              $my_query = new WP_Query($args);
            ?>
            <?php if ( $my_query->have_posts() ) : ?>
              <?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
                <a href="#">
                  <div class="detailblock">
                    <div class="date"><?php the_time('Y.m.d'); ?></div>
                    <div class="text"><?php the_title(); ?></div>
                  </div>
                </a>
              <?php endwhile; ?>
            <?php endif; ?>
          </div>
          <div class="more"><a href="#">もっと見る</a></div>
        </div>
      </div>
      <div class="columns">
        <div class="container"> 
          <div class="title">
            <p>COLUMN</p>
          </div>
          <div class="contents">
            <?php
              $args = [
                'post_type' => 'column', // カスタム投稿名
                'posts_per_page' => 6, // 表示する数
              ];
              $my_query = new WP_Query($args);
            ?>
            <?php if ( $my_query->have_posts() ) : ?>
              <?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
                <a class="column" href="#">
                  <div class="column__image">
                    <?php the_post_thumbnail('thumbnail',array('class' => 'column-image')); ?>
                  </div>
                  <div class="column__explanation">
                    <div class="column__explanation--date"><?php the_time('Y.m.d'); ?></div>
                    <div class="column__explanation--text"><?php the_title(); ?></div>
                  </div>
                </a>
              <?php endwhile; ?>
            <?php endif; ?>
            
          </div>
          <div class="more"><a href="#">もっと見る</a></div>
        </div>
      </div>
      <div class="message">
        <div class="container">
          <div class="title"> 
            <p>豊富な法律知識と実績から</p>
            <p>最善の解決方法を</p>
            <p>ご提案します</p>
          </div>
          <div class="detail">
            <p>税理士は各種税金・税務申告に関する問題をはじめ、</p>
            <p>会社の経理や経営相談、相続に代表されるご家族の悩みなどを相談できる専門家です。</p>
            <p>相続問題、独立・企業をお考えの方へのサポート、法人・個人事業主の税務申告・</p>
            <p>会計業務と経営支援を業務の中心として取り組んでおります。</p>
            <p>フットワーク軽く、一緒に考え、一緒に悩み、皆様の不安を解決して参ります。</p>
            <p>一人で悩まず、まずはご相談下さい。</p>
          </div>
        </div>
      </div>
      <div class="contact">
        <div class="container"><a href="#">お問い合わせはこちら</a></div>
      </div>
      <div class="map">
        <div class="container">
          <div class="title">
            <p>アクセス</p>
          </div>
          <div class="iframe">
            <iframe class="googlemap" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3239.1406546211765!2d139.70965201525996!3d35.72275918018463!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188d6a6d9adf75%3A0xf09c2e010c44aa19!2z5qCq5byP5Lya56S-44Kz44Og44OH!5e0!3m2!1sja!2sjp!4v1607303824497!5m2!1sja!2sjp" frameborder="0"></iframe>
          </div>
          <div class="explanation">
            <p>JR線「吉祥寺駅」から徒歩7分</p>
          </div>
        </div>
      </div>
      <div class="to-top"></div>
    <?php get_footer(); ?>