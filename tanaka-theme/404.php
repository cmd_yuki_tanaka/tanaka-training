<?php get_header(); ?>
<div style="
  width:100vw;
  height:300px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;">
  <h1 style="margin-bottom: 40px">お探しのページが見つかりませんでした。</h1>
  <a href="<?php echo home_url('/'); ?>"><h2>トップページへ戻る</h2></a>
</div>
<?php get_footer(); ?>