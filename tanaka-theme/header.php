<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/styles/styles.css">
    <link rel="stylesheet" href="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/styles/question.css">
    <link rel="shortcut icon" href="<?php get_template_directory_uri() ?>/assets/images/favicon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
      wp_deregister_script('jquery'); // デフォルトのjQueryを読み込まない
      wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', '', '3.3.1', true );
      wp_enqueue_style('swiper', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/5.3.7/css/swiper.min.css');
      wp_head();
    ?>
  </head>
  <body <?php body_class(); ?>>
    <div id="wrapper">
      <header class="header">
        <div class="container">
          <div class="leftcontent"><a href="<?php echo home_url(); ?>"><img class="site" src="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/images/sitelogo.png" alt="堤税理士事務所"></a></div>
          <div class="rightcontent">
            <div class="rightcontent__contact">
              <div class="rightcontent__contact--phone">
                <p>お気軽にお問い合わせください</p><span>0123-456-789</span>
                <p>お電話受付時間00:00〜00:00</p>
              </div>
              <div class="rightcontent__contact--form"><a class="contact-button" href="#">まずは無料相談・お問い合わせ＞</a></div>
            </div>
            <nav class="rightcontent__nav">
              <ul>
                <li><a href="#">相続のご相談</a></li>
                <li><a href="#">顧問税理士をお探しの方</a></li>
                <li><a href="#">ご依頼の流れ</a></li>
                <li><a href="<?php echo get_permalink(62); ?>">よくあるご質問</a></li>
                <li><a href="#">事務所案内</a></li>
                <li><a href="#">お問い合わせ</a></li>
              </ul>
            </nav>
          </div>
          <button class="nav-button" onClick="navFunc()"></button>
        </div>
        <div class="mobile-nav">
          <ul>
            <li><a href="#">相続のご相談</a></li>
            <li><a href="#">顧問税理士をお探しの方</a></li>
            <li><a href="#">ご依頼の流れ</a></li>
            <li><a href="<?php echo get_permalink(62); ?>">よくあるご質問</a></li>
            <li><a href="#">事務所案内</a></li>
            <li><a href="#">お問い合わせ</a></li>
          </ul>
        </div>
      </header>