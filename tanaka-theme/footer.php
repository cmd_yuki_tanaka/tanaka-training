      <footer class="footer">
        <nav class="footer-nav">
          <ul>
            <li><a href="#">相続のご相談</a></li>
            <li><a href="#">顧問税理士をお探しの方</a></li>
            <li><a href="#">ご依頼の流れ</a></li>
            <li><a href="<?php echo get_permalink(62); ?>">よくあるご質問</a></li>
            <li><a href="#">事務所案内</a></li>
            <li><a href="#">お問い合わせ</a></li>
            <li><a href="#">プライバシーポリシー</a></li>
            <li><a href="#">コラム</a></li>
          </ul>
        </nav>
        <div class="container">
          <div class="office-name"><a href="<?php echo home_url(); ?>"><img src="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/images/sitelogo.png" alt="堤税理士事務所"></a></div>
          <div class="office-info"> 
            <p>〒180-0002 東京都武蔵野市吉祥寺東町1丁目1番18号</p>
            <p>TEL 0422-21-3611</p>
          </div>
        </div>
      </footer>
    </div>
    <?php
      // Swiper
      wp_enqueue_script('swiper', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/5.3.7/js/swiper.min.js');
      wp_enqueue_script('myswiper', get_template_directory_uri() . '/assets/javascripts/swiper.js');
      // ページ上部へ戻るボタンのJS
      wp_enqueue_script('toppage', get_template_directory_uri() . '/assets/javascripts/toppage.js');
    ?>
    <?php wp_footer(); ?>
  </body>
</html>