    <?php get_header(); ?>
      <div class="page-title">
        <div class="container">
          <h1>よくあるご質問</h1>
        </div>
      </div>
      <div class="bread-list">
        <div class="container"><a href="<?php echo home_url(); ?>">トップ</a>
          <figure>＞</figure><a href="<?php echo get_permalink(62); ?>">よくある質問</a>
        </div>
      </div>
      <div class="anchor-links">
        <div class="container"><a class="link" href="#inheritance-tax">相続税について </a><a class="link" href="#advisory-contract">顧問契約について </a></div>
      </div>
      <div class="questions" id="inheritance-tax">
        <div class="container">
          <div class="subtitle"> 
            <p>相続税について</p>
          </div>
          <div class="contents">
            <div class="inner">
              <figure><img class="q-image" src="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/images/question.png" alt=""></figure>
              <div class="text"> 
                <p>
                  原稿はダミーです、テキストテキストテキストテテキストテキストキストテキストテキストテキストテキスト。<br>
                  テキストテキストテキストテキストテテキストテキストキスト。<br>
                  テキストテキストテキストテキストテキステキストテキストテキストテキスト。 （よくある質問）
                </p>
              </div>
            </div>
          </div>
          <div class="contents red-bgc">
            <div class="inner">
              <figure><img class="a-image" src="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/images/answer.png" alt=""></figure>
              <div class="text">
                <p>
                  原稿はダミーです、テキストテキストテキストテテキスト。<br>
                  テキストテキストテキストテキストテテキストテキストテキストキストテキストテキストテキストテキストキスト。<br>
                  テキストテキストテキストテキステキストテキストテキストテキスト。  （質問への回答）
                </p>
              </div>
            </div>
          </div>
          <div class="contents">
            <div class="inner">
              <figure><img class="q-image" src="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/images/question.png" alt=""></figure>
              <div class="text">
                <p>
                  原稿はダミーです、テキストテキストテキストテキストテキストテキストテキストテキスト。<br>
                  テキストテキストテキストテキストテキストテキストテキストテキストテキステキストテキストテキストテキスト。 （よくある質問）
                </p>
              </div>
            </div>
          </div>
          <div class="contents red-bgc">
            <div class="inner">
              <figure><img class="a-image" src="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/images/answer.png" alt=""></figure>
              <div class="text">
                <p>
                  原稿はダミーです、テキストテキストテキストテテキスト。テキストテキストテキストテキストテテキストテキストテキスト<br>
                  テキストテキストキストテキストテキストテキストテキストキスト。<br>
                  テキストテキストテキストテキステキストテキストテキステキストテキストトテキスト。  （質問への回答）
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="questions" id="advisory-contract">
        <div class="container">
          <div class="subtitle"> 
            <p>顧問契約について</p>
          </div>
          <div class="contents">
            <div class="inner">
              <figure><img class="q-image" src="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/images/question.png" alt=""></figure>
              <div class="text"> 
                <p>
                  原稿はダミーです、テキストテキストテキストテテキストテキストキストテキストテキストテキストテキスト。<br>
                  テキストテキストテキストテキストテテキストテキストキスト。<br>
                  テキストテキストテキストテキストテキステキストテキストテキストテキスト。 （よくある質問）
                </p>
              </div>
            </div>
          </div>
          <div class="contents red-bgc">
            <div class="inner">
              <figure><img class="a-image" src="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/images/answer.png" alt=""></figure>
              <div class="text">
                <p>
                  原稿はダミーです、テキストテキストテキストテテキスト。<br>
                  テキストテキストテキストテキストテテキストテキストテキストキストテキストテキストテキストテキストキスト。<br>
                  テキストテキストテキストテキステキストテキストテキストテキスト。  （質問への回答）
                </p>
              </div>
            </div>
          </div>
          <div class="contents">
            <div class="inner">
              <figure><img class="q-image" src="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/images/question.png" alt=""></figure>
              <div class="text">
                <p>
                  原稿はダミーです、テキストテキストテキストテキストテキストテキストテキストテキスト。<br>
                  テキストテキストテキストテキストテキストテキストテキストテキストテキステキストテキストテキストテキスト。 （よくある質問）
                </p>
              </div>
            </div>
          </div>
          <div class="contents red-bgc">
            <div class="inner">
              <figure><img class="a-image" src="<?php get_template_directory_uri() ?>/wtanakap/wp-content/themes/tanaka-training/assets/images/answer.png" alt=""></figure>
              <div class="text">
                <p>
                  原稿はダミーです、テキストテキストテキストテテキスト。テキストテキストテキストテキストテテキストテキストテキスト<br>
                  テキストテキストキストテキストテキストテキストテキストキスト。<br>
                  テキストテキストテキストテキステキストテキストテキステキストテキストトテキスト。  （質問への回答）
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php get_footer(); ?>